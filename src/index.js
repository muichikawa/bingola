import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

let values = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
            "30", "31", "32", "33"];
values.push("VERSION 3.0", "LEVEL UP", "#YOLO", "AM I OLD NOW?", "FOREVER YOUNG", "SECOND YOUTH", "MID LIFE CRISIS", "30 IS THE NEW 20", "18 Y.O. WITH EXP", "25 + 20% VAT");

function Square(props) {
        let className = "square " + props.value;
        if (props.clicked) {
            className += " clicked";
        }
        return (
            <button
                className={className}
                onClick={props.onClick}
            >
                <SquareContent isImage={isFinite(props.value) && props.value < 100} value={props.value} />
            </button>
    );
}

function SquareContent(props) {
    if (props.isImage) {
        return (
            <img src={"./resources/" + props.value + ".png"} alt={props.value} className="image" />
        );
    } else {
        return (
          <div className="text-value">
              <span>{props.value}</span>
          </div>
        );
    }
}

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            squares: Array(25).fill(false),
            currentBoard: Array(25).fill(null),
            usedCellValues: [],
            lastWinningConditions: [],
            status: false
        };
    }

    handleClick(i) {
        const squares = this.state.squares.slice();
        squares[i] = !squares[i];

        const winningConditions = calculateWinningCondition(squares);
        let status = winningConditions && winningConditions.length > 0 && winningConditions.length > this.state.lastWinningConditions.length
            && winningConditions.some(r => this.state.lastWinningConditions.indexOf(r) < 0);

        this.setState({
            squares: squares,
            lastWinningConditions: winningConditions,
            status: status,
        });
    }

    renderSquare(position) {
        this.getRandomNonRepeatingValue(position);

        return (
            <Square
                value={this.state.currentBoard[position]}
                clicked={this.state.squares[position]}
                onClick={() => this.handleClick(position)}
            />
        );
    }

    render() {
        let statusValue = this.state.status? "BINGOLA!": "";
        return (
            <div>
                <div className="status">{statusValue}</div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                </div>
                <div className="board-row">
                    {this.renderSquare(5)}
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                    {this.renderSquare(9)}
                </div>
                <div className="board-row">
                    {this.renderSquare(10)}
                    {this.renderSquare(11)}
                    {this.renderSquare(12)}
                    {this.renderSquare(13)}
                    {this.renderSquare(14)}
                </div>
                <div className="board-row">
                    {this.renderSquare(15)}
                    {this.renderSquare(16)}
                    {this.renderSquare(17)}
                    {this.renderSquare(18)}
                    {this.renderSquare(19)}
                </div>
                <div className="board-row">
                    {this.renderSquare(20)}
                    {this.renderSquare(21)}
                    {this.renderSquare(22)}
                    {this.renderSquare(23)}
                    {this.renderSquare(24)}
                </div>
            </div>
        );
    }

    getRandomNonRepeatingValue(position) {
        if (this.state.currentBoard[position]) {
            return this.state.currentBoard[position];
        }

        for (let i = 0; i < values.length; i++) {
            let value = values[Math.floor(Math.random() * values.length)];
            if (!this.state.usedCellValues.includes(value)) {
                this.state.usedCellValues.push(value);

                let newCurrentBoard = this.state.currentBoard;
                newCurrentBoard[position] = value;
                this.setState({currentBoard: newCurrentBoard});

                return value;
            }
        }
    }
}

class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board />
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
    );
    }
}

// ========================================

ReactDOM.render(
<Game />,
    document.getElementById('root')
);

function calculateWinningCondition(squares) {
    const lines = [
        [ 0,  1,  2,  3,  4],
        [ 5,  6,  7,  8,  9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24],
        [ 0,  5, 10, 15, 20],
        [ 1,  6, 11, 16, 21],
        [ 2,  7, 12, 17, 22],
        [ 3,  8, 13, 18, 23],
        [ 4,  9, 14, 19, 24],
        [ 0,  6, 12, 18, 24],
        [20, 16, 12,  8,  4],
    ];
    let winningCombinations = [];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c, d, e] = lines[i];
        if (squares[a] && squares[b] && squares[c] && squares[d] && squares[e]) {
            winningCombinations.push(i);
        }
    }
    return winningCombinations;
}